#ifndef   STORAGE_H
#define   STORAGE_H

#define NO_GLOBAL_INSTANCES
#include <stdint.h>
#include <EEPROM.h>

class Storage {
    public:
      Storage();
      void saveLevels(uint16_t * levels, char channels);
      void readLevels(uint16_t * levels);
      void setup();
    private:
      EEPROMClass * EEPROM;
};

#endif